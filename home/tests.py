from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from .views import index

# Create your tests here

class UrlsTest(TestCase):
    def setUp(self):
        self.index = reverse("home:index")
    
    def test_index_use_right_function(self):
        found = resolve(self.index)
        self.assertEqual(found.func, index)

class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.index = reverse("home:index")
    
    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "home/index.html")