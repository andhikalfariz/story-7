// $(".title").click(function() {
//     $(this).parent().next().slideToggle();
// })


// $(".up").click(function() {
//     const current = $(this).parent().parent().parent()
//     const currentClass = 'accordion-'+current.css('order')
//     const previousClass = 'accordion-'+ parseInt(current.css('order') - 1)
//     if (perseInt(current.css('order')) > 1) {
//         $('-' + previousClass).addClass(currentClass)
//         $('-' + previousClass).removeClass(previousClass)
//         $(this).parent().parent().parent().removeClass(currentClass)
//         $(this).parent().parent().parent().addClass(previousClass)
//     }
// })

// $(".down").click(function() {
//     const current = $(this).parent().parent().parent()
//     const currentClass = 'accordion-'+current.css('order')
//     const nextClass = 'accordion-'+ parseInt(current.css('order') + 1)
//     if (perseInt(current.css('order')) < 5) {
//         $('-' + nextClass).addClass(currentClass)
//         $('-' + nextClass).removeClass(nextClass)
//         $(this).parent().parent().parent().removeClass(currentClass)
//         $(this).parent().parent().parent().addClass(nextClass)
//     }
// })


// // $(".down").click(function(){
// //     $("accordion").insertAfter("p");
// //     });


$(document).ready(function() {
    var panels = $('.accordion .description').hide();
    var prev = null;

    $('.accordion .content .title a').click(function() {
        var $this = $(this);

        if (this == prev) {
            $this.parent().next().slideToggle();
        } else {
            // slide every panels
            panels.slideUp();
            // slide down target panel
            $this.parent().next().slideDown();
        }
        prev = this;

        return false;
    });

    $('.accordion > .content > .title > .upbutton').click(function() {
        var divPointed = $(this).parent().parent();
        divPointed.after(divPointed.prev());
        return false;
    });

    $('.accordion > .content > .title > .downbutton').click(function() {
        var divPointed = $(this).parent().parent();
        divPointed.before(divPointed.next());
        return false;
    });
});
